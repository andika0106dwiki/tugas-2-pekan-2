<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>
<body>
    <h1>Berlatih Array</h1>
        
    <?php 
        echo "<h3> Soal 1 </h3>";
        /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
        $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"]; // Lengkapi di sini
        
        $adults= ["Hopper", "Nancy", "Joyce", "Jonathan", "Murray"]; 
        
        echo "Kids = ";
        foreach ($kids as $kid) {
            echo $kid.", ";
        }
        echo "<br>Adults = ";
        foreach($adults as $adult){
            echo $adult.", ";
        }
        echo "<h3> Soal 2</h3>";
        /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
        echo "Cast Stranger Things: ";
        echo "<br><br>";
        $jumlahAnak = count($kids);
        echo "Total Kids: ".$jumlahAnak ; // Berapa panjang array kids
        echo "<br>";
        echo "<ol>"; 
        foreach ($kids as $kid) {
            echo "<li> $kid </li>";
        }
        // Lanjutkan

        echo "</ol>";
        $jumlahDewasa = count($adults);
        echo "Total Adults: ".$jumlahDewasa;// Berapa panjang array adults
        echo "<br>";
        echo "<ol>";
        foreach($adults as $adult){
            echo "<li> $adult </li>";
        }
        
        // Lanjutkan

        echo "</ol>";

        /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array (Array Multidimensi)
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"
            
        */
        echo "<h3> Soal 3</h3>";

        $datadata = [
            [   
                "name" => "Will Byers",
                "age" => 12,
                "aliases" => "Dungeon Master",
                "status" => "Alive"
            ],
            [
                "name" => "Mike",
                "age" => 12,
                "aliases" => "Dungeon Master",
                "status" => "Alive"
            ],
            [
                "name" => "Jim Hopper",
                "age" => 43,
                "aliases" => "Chief Hopper",
                "status" => "Deceased"
            ],
            [
                "name" => "Eleven",
                "age" => 12,
                "aliases" => "El",
                "status" => "Alive"
            ]
        ];
        echo  "<pre>";
        print_r($datadata);
        echo "</pre>";
            foreach($datadata as $data){
                echo "<ul>";
                echo "<li>";
                echo "Name : ".$data["name"]."<br>";
                echo "Age : ".$data["age"]."<br>";
                echo "Aliases : ".$data["aliases"]."<br>";
                echo "Status : ".$data["status"]."<br><br>";
                echo "</li>";
                echo "</ul>";
            }
    ?>
</body>
</html>